Rails.application.routes.draw do
  resources :stories
  resources :users
  get '/stories/userstory/:user_id', to: 'stories#userstory', as: 'userstory_stories'
end
